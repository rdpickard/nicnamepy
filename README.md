# nicnamepy

Module for getting WHOIS information for a domain as described in [RFC 3912](https://tools.ietf.org/html/rfc3912). In the original proposal the protocol was called 'nicname'  