# The MIT License (MIT)
# Copyright (c) 2019 Robert Daniel Pickard
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
# OR OTHER DEALINGS IN THE SOFTWARE.

"""
nicnamepy

Perform a WHOIS lookup for a a domain based on the RFC 3912
https://tools.ietf.org/html/rfc3912
"""

import socket
import logging
import unittest
import pprint
import re
import base64
import time


__author__ = "Robert Daniel Pickard"
__credits__ = ["Robert Daniel Pickard"]
__license__ = "MIT"
__maintainer__ = "Robert Daniel Pickard"
__email__ = "codez+nicnamepy@chalkfarm.mx"
__status__ = "caveat emptor"


def guess_whois_server_for_domain(domain_name: str,
                                  logger=logging.getLogger("nicname")):
    """
    Provide the appropriate server to query for a specified domain.

    The WHOIS standard does not specify how responsibility for providing domain information is to be delegated to
    different institutions. Some top level domains (org, com, uk, nyc, net, etc) have well known WHOIS servers but
    those servers are only well-known by convention. Some domain information returned by a WHOIS server may 'point'
    to another WHOIS server, such as a registrar, that has more detailed information about the domain. The method
    of 'pointing' to another WHOIS server is not standardized.

    This method attempts to traverse the informal hierarchy of WHOIS servers in a way to find the WHOIS server
    with the most detailed information.

    Some WHOIS servers, notably markmonitor.com, will throttle the rate of requests to it's network from a client. This
    method will return the last response from the request for WHOIS data if it is available. A caller can then avoid
    making redundant calls to a WHOIS server. If the best WHOIS server for domain_name does not require traversing a
    hierarchy of responses from various servers, this response data will be None.

    For example if the method needs to make a request to iana.whois.org and gets a response that points to Verisign
    which in turn points to markmonitor.com the return value will be "markmonitor.com" and teh response from the
    query to markmonitor.

    :param domain_name: The domain to look identify the most approriate WHOIS server for
    :param logger: Logger to use. Defaults to 'nicname'
    :returns: The WHOIS server to use as a string and possibly the response data from that server for the domain_name.
    The response data may be None. If response data is not None the caller of this function can expect the data to
    be a copy of the most detailed WHOIS record available.
    """

    # TODO Add the 'well known' WHOIS servers for TLDs

    whois_host = "whois.iana.org"  # Start by trying IANA
    previous_whois_host = None

    whois_blob = None  # The blob of text that is the response for a domain query

    while True:
        # Loop through the responses to requests for information about domain_name looking for pointers
        # to more detailed records. If there is no pointer to a more specific set of records break out of
        # the loop. The last server before the break (the one with the most specific response) will be
        # the server returned to the caller
        next_whois_host = None
        logger.info(f"{previous_whois_host}<-{whois_host}->{next_whois_host}")

        try:
            whois_blob = lookup_domain_by_name(domain_name, whois_host, logger)
            if len(whois_blob) == 0:
                # Some WHOIS servers return with no data for domains they don't know about. Interpret this as
                # having gone too far. Break out of the loop
                break
        except ConnectionError or ConnectionResetError as ce:
            logger.info(f"Could not query {whois_host} for {domain_name} due to connection error '{ce}'")
            whois_host = previous_whois_host
        except Exception as e:
            logger.info(f"SUPER Could not query {whois_host} for {domain_name} due to connection error '{e}'")
            break

        characteristics = {}
        for line in filter(lambda cline: cline.find(":") > 0, whois_blob.splitlines()):
            characteristic, value = map(lambda v: v.strip(), line.split(":", 1))
            characteristics[characteristic] = value
        if 'Registrar WHOIS Server' in characteristics.keys():
            # One way a pointer to a more specific WHOIS server is specified in a response
            next_whois_host = characteristics['Registrar WHOIS Server']
        elif 'whois' in characteristics.keys():
            # An alternative pointer to a more specific WHOIS server is specified in a response
            next_whois_host = characteristics['whois']
        else:
            break

        if next_whois_host is None:
            # The current response did not point to a more specific WHOIS server. This is as specific as it gets
            break
        elif next_whois_host.lower() == whois_host.lower():
            # Some responses will point to themselves. Treat these cases as being the same as no more specific
            # server than the current one
            break
        else:
            # There is a more specific server indicated in the current WHOIS response. Continue the loop
            previous_whois_host = whois_host
            whois_host = next_whois_host

    return whois_host, whois_blob


def parse_to_json(response_blob, logger=logging.getLogger("nicname")):
    """
    There isn't a standard response format for WHOIS request responses. Make a best effort to extract interesting
    information from the response data into a normalized format.

    For example ".mx" records represents the DNS name server information as

    '
    Name Servers:
        DNS:            ns-1602.awsdns-08.co.uk
        DNS:            ns-1310.awsdns-35.org
        DNS:            ns-668.awsdns-19.net
        DNS:            ns-668.awsdns-19.net
    '

    the DNS name server information for the '.org' top level domain is represented


    '
    Name Server: NS-1132.AWSDNS-13.ORG
    Name Server: NS-1670.AWSDNS-16.CO.UK
    Name Server: NS-297.AWSDNS-37.COM
    Name Server: NS-828.AWSDNS-39.NET
    '

    This function will find look for known ways the data is represented in the response and add the values to the
    returned dictionary under the key 'name_servers' irrespective of how it was represented in the the WHOIS response.
    Using the examples above the returned dictionaries would have key values

    {'name_server': ['ns-1602.awsdns-08.co.uk', 'ns-1310.awsdns-35.org', 'ns-668.awsdns-19.net', 'ns-668.awsdns-19.net'}

    {'name_server': ['NS-1132.AWSDNS-13.ORG', 'NS-1670.AWSDNS-16.CO.UK', 'NS-297.AWSDNS-37.COM', 'NS-828.AWSDNS-39.NET'}

    respectively. See the return section of the documentation for the the keys of the dictionary


    :param response_blob: string of unprocessed response from WHOIS server
    :param logger: Logger to use. Defaults to logger for 'nicname'
    :return: Dictionary of characteristics of the domain
    """
    whois_information = {
        'name_servers': None,
        'technical_contact': None,
        'administrative_contact': None,
        'domain_name': None,
        'is_not_registered': None,
        'registrar': None,
        'created_date': None,
        'updated_date': None,
        'expires_date': None,
        'dns_sec': None,
        'registrant': None
    }

    logger.debug("Parsing blob {}".format((base64.b64encode(bytearray(response_blob, "utf-8")))))

    def strip_it(list_to_strip):
        return None if len(list_to_strip) < 1 else list(map(lambda element: element.strip(), list_to_strip))

    def first_or_none(list_to_pop):
        return None if list_to_pop is None or len(list_to_pop) < 1 else list_to_pop[0]

    whois_response_information_parsers = {
        'name_servers': [
            lambda text: strip_it(re.findall(r'Name Server: ([a-zA-Z0-9.-]*)', text)),  # works with org,com,net
            lambda text: strip_it(re.findall(r'DNS: ([a-zA-Z0-9.\- ]*)', text)),  # works with .mx,
            lambda text: strip_it(re.findall(r'nserver: ([a-zA-Z0-9.-]*)', text)),  # works with .mx
            lambda text: strip_it([] if re.search(r'Name servers:\r\n(.*?)\r\n\r\n', text, re.DOTALL) is None else
                                  re.search(r'Name servers:\r\n(.*?)\r\n\r\n', text, re.DOTALL)[1])  # works with .mx

        ],
        'technical_contact': [
            lambda text: re.findall(r'Tech (.*?): (.*?)\n', text)

        ],
        'administrative_contact': [
            lambda text: re.findall(r'Admin (.*?): (.*?)\n', text)
        ],
        'billing_contact': [

        ],
        'registrant': [
            lambda text: re.findall(r'Registrant (.*?): (.*?)\n', text)
        ],
        'domain_name': [
            lambda text: first_or_none(strip_it(re.findall(r'Domain Name: (.*?)\r\n', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Domain Name: (.*?)\n', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Domain [N|n]ame:(.*?)\r\n\r\n', text, re.DOTALL))),
        ],
        'is_not_registered': [
            lambda text: None if len(re.findall(r'No match for "(.*?)"\.', text)) < 1 else True,
            lambda text: None if len(re.findall(r'^NOT FOUND(.*)', text)) < 1 else True
        ],
        'registrar': [
            lambda text: first_or_none(strip_it(re.findall(r'Registrar: ([a-zA-Z0-9.\- ]*)', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Registrar:(.*?)\r\n\r\n', text, re.DOTALL)))
        ],
        'created_date': [
            lambda text: first_or_none(strip_it(re.findall(r'Creation Date:(.*)', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Created On:(.*)', text))),
            lambda text: first_or_none(
                strip_it(re.findall(r'Relevant dates:\r\n.*?Registered on:(.*?)\r\n', text, re.DOTALL)))
        ],
        'updated_date': [
            lambda text: first_or_none(strip_it(re.findall(r'Updated Date:(.*)', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Last Updated On:(.*)', text))),
            lambda text: first_or_none(
                strip_it(re.findall(r'Relevant dates:\r\n.*?Last updated:(.*?)\r\n', text, re.DOTALL)))
        ],
        'expires_date': [
            lambda text: first_or_none(strip_it(re.findall(r'Registry Expiry Date:(.*)', text))),
            lambda text: first_or_none(strip_it(re.findall(r'Expiration Date:(.*)', text))),
            lambda text: first_or_none(
                strip_it(re.findall(r'Relevant dates:\r\n.*?Expiry date:(.*?)\r\n', text, re.DOTALL)))
        ],
        'dns_sec': []
    }

    for characteristic in whois_information.keys():
        i = 0
        for parser in whois_response_information_parsers[characteristic]:
            parser_result = parser(response_blob)
            if parser_result is not None:
                whois_information[characteristic] = parser_result
                break
            i += 1

    logger.debug("Formatted whois_information {}".format(base64.b64encode(bytearray(pprint.pformat(whois_information),
                                                                          "utf-8"))))
    return whois_information


def lookup_domain_by_name(domain_name: str, whois_server=None, logger=logging.getLogger("nicname")):
    """

    :param domain_name:
    :param whois_server:
    :param logger:
    :return:
    """
    if whois_server is None:
        whois_server, whois_blob = guess_whois_server_for_domain(domain_name)
        if whois_blob is not None:
            return whois_blob

    if whois_server is None:
        logger.warning(f"Can't determine WHOIS server for domain '{domain_name}'. Bailing")
        return None

    response_bytes = None

    whois_port = 43
    logger.debug(f"Looking up WHOIS for {domain_name} from server {whois_server}")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    err = None
    try:
        s.connect((whois_server, whois_port))
        total_bytes_sent = 0
        while total_bytes_sent < len(domain_name):
            sent = s.send(domain_name.encode('utf-8')[total_bytes_sent:])
            if sent == 0:
                msg = f"Socket connect to {whois_server}:{whois_port} broken after sending {total_bytes_sent}"
                logging.error(msg)
                raise RuntimeError(msg)
            total_bytes_sent += sent
        s.send("\r\n".encode('utf-8'))

        response_bytes = bytearray()

        while True:
            current_response = s.recv(4096)
            if len(current_response) == 0 or s.fileno() == -1:
                break
            response_bytes.extend(current_response)
    except Exception as e:
        time.sleep(1)
        logger.exception(e)
        err = e
    finally:
        s.close()
        if err is not None:
            raise err

    response_blob = response_bytes.decode("utf-8")
    return response_blob


class TestNicNameLookup(unittest.TestCase):

    tlds_and_domains = {
        "com": ["amazon.com", "google.com", "netscout.com"],
        "org": ["chalkfarm.org"],
        "mx": ["chalkfarm.mx"],
        "co_uk": ["amazon.co.uk", "google.co.uk"]
    }

    for tld, domains in tlds_and_domains.items():
        for domain in domains:
            domain_method_name = f'test_tld_{tld}_{domain.replace(".","DOT")}'

            test_code = f"""
def {domain_method_name}(self):

    response_blob = lookup_domain_by_name("{domain}")
    self.assertIsNotNone(response_blob, "Lookup for {domain} returned None")
    self.assertIsInstance(response_blob, str, "Lookup for {domain} did not return string")

    domain_whois_characteristics = parse_to_json(response_blob)
    self.assertIsNotNone(domain_whois_characteristics, "Parse for {domain} returned None")
    self.assertIsInstance(response_blob, str, "Parse for domain did not return dictionary")

    self.assertFalse(domain_whois_characteristics['is_not_registered'], f"{domain} is not registered")
    self.assertIsNotNone(domain_whois_characteristics['created_date'], f"{domain} no created date")
    self.assertIsNotNone(domain_whois_characteristics['updated_date'], f"{domain} no updated date")
    self.assertIsNotNone(domain_whois_characteristics['expires_date'], f"{domain} no expires date")
    self.assertIsNotNone(domain_whois_characteristics['name_servers'], f"{domain} no name servers")
    self.assertIsNotNone(domain_whois_characteristics['registrar'], f"{domain} no registrar")
    self.assertIsNotNone(domain_whois_characteristics['domain_name'], f"{domain} no domain name in response")

"""
            exec(test_code)

    @classmethod
    def setUpClass(cls) -> None:
        # Setup a logger that will output debug messages
        formatter = logging.Formatter('%(asctime)s - %(name)s:%(funcName)s - %(levelname)s - %(message)s')
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(formatter)
        logger = logging.getLogger("nicname")
        logger.addHandler(ch)
        logger.setLevel(logging.DEBUG)
